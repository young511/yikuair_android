# view res/layout/pager.xml #generated:7
# view res/layout/schedule.xml #generated:192
# view res/layout/schedule_add.xml #generated:143
-keep class android.support.v4.view.ViewPager { <init>(...); }

# view res/layout/abs__screen_action_bar.xml #generated:43
# view res/layout/abs__screen_action_bar_overlay.xml #generated:26
# view res/layout/abs__screen_simple.xml #generated:32
# view res/layout/abs__screen_simple_overlay_action_mode.xml #generated:27
# view xlarge/res/layout-xlarge/abs__screen_action_bar.xml #generated:43
# view xlarge/res/layout-xlarge/abs__screen_action_bar_overlay.xml #generated:24
-keep class com.actionbarsherlock.internal.nineoldandroids.widget.NineFrameLayout { <init>(...); }

# view large/res/layout-large/abs__action_mode_close_item.xml #generated:17
# view res/layout/abs__action_mode_close_item.xml #generated:17
-keep class com.actionbarsherlock.internal.nineoldandroids.widget.NineLinearLayout { <init>(...); }

# view res/layout/abs__action_menu_item_layout.xml #generated:17
-keep class com.actionbarsherlock.internal.view.menu.ActionMenuItemView { <init>(...); }

# view res/layout/abs__action_menu_layout.xml #generated:17
-keep class com.actionbarsherlock.internal.view.menu.ActionMenuView { <init>(...); }

# view res/layout/abs__list_menu_item_layout.xml #generated:17
# view res/layout/abs__popup_menu_item_layout.xml #generated:17
-keep class com.actionbarsherlock.internal.view.menu.ListMenuItemView { <init>(...); }

# view res/layout/abs__screen_action_bar.xml #generated:26
# view res/layout/abs__screen_action_bar.xml #generated:50
# view res/layout/abs__screen_action_bar_overlay.xml #generated:29
# view res/layout/abs__screen_action_bar_overlay.xml #generated:52
# view xlarge/res/layout-xlarge/abs__screen_action_bar.xml #generated:26
# view xlarge/res/layout-xlarge/abs__screen_action_bar_overlay.xml #generated:27
-keep class com.actionbarsherlock.internal.widget.ActionBarContainer { <init>(...); }

# view res/layout/abs__action_mode_bar.xml #generated:19
# view res/layout/abs__screen_action_bar.xml #generated:36
# view res/layout/abs__screen_action_bar_overlay.xml #generated:40
# view xlarge/res/layout-xlarge/abs__screen_action_bar.xml #generated:36
# view xlarge/res/layout-xlarge/abs__screen_action_bar_overlay.xml #generated:37
-keep class com.actionbarsherlock.internal.widget.ActionBarContextView { <init>(...); }

# view res/layout/abs__screen_action_bar.xml #generated:31
# view res/layout/abs__screen_action_bar_overlay.xml #generated:35
# view xlarge/res/layout-xlarge/abs__screen_action_bar.xml #generated:31
# view xlarge/res/layout-xlarge/abs__screen_action_bar_overlay.xml #generated:32
-keep class com.actionbarsherlock.internal.widget.ActionBarView { <init>(...); }

# view res/layout/abs__action_bar_home.xml #generated:17
-keep class com.actionbarsherlock.internal.widget.ActionBarView$HomeView { <init>(...); }

# view res/layout/abs__action_menu_item_layout.xml #generated:40
-keep class com.actionbarsherlock.internal.widget.CapitalizingButton { <init>(...); }

# view res/layout/abs__dialog_title_holo.xml #generated:23
-keep class com.actionbarsherlock.internal.widget.FakeDialogPhoneWindow { <init>(...); }

# view res/layout/abs__action_bar_tab_bar_view.xml #generated:3
# view res/layout/abs__activity_chooser_view.xml #generated:19
-keep class com.actionbarsherlock.internal.widget.IcsLinearLayout { <init>(...); }

# view res/layout/abs__action_bar_tab.xml #generated:3
-keep class com.actionbarsherlock.internal.widget.ScrollingTabContainerView$TabView { <init>(...); }

# view res/layout/abs__search_view.xml #generated:84
-keep class com.actionbarsherlock.widget.SearchView$SearchAutoComplete { <init>(...); }

# view AndroidManifest.xml #generated:242
-keep class com.baidu.android.pushservice.PushService { <init>(...); }

# view AndroidManifest.xml #generated:218
-keep class com.baidu.android.pushservice.PushServiceReceiver { <init>(...); }

# view AndroidManifest.xml #generated:228
-keep class com.baidu.android.pushservice.RegistrationReceiver { <init>(...); }

# view AndroidManifest.xml #generated:201
-keep class com.crittercism.NotificationActivity { <init>(...); }

# view AndroidManifest.xml #generated:42
-keep class com.slidingmenu.example.AppstartActivity { <init>(...); }

# view AndroidManifest.xml #generated:69
-keep class com.slidingmenu.example.AttachExample { <init>(...); }

# view AndroidManifest.xml #generated:66
-keep class com.slidingmenu.example.LeftAndRightActivity { <init>(...); }

# view AndroidManifest.xml #generated:54
-keep class com.slidingmenu.example.LoginActivity { <init>(...); }

# view AndroidManifest.xml #generated:63
-keep class com.slidingmenu.example.PropertiesActivity { <init>(...); }

# view res/layout/menu.xml #generated:2
-keep class com.slidingmenu.example.SampleListFragment { <init>(...); }

# view AndroidManifest.xml #generated:87
-keep class com.slidingmenu.example.SlidingContent { <init>(...); }

# view AndroidManifest.xml #generated:84
-keep class com.slidingmenu.example.SlidingTitleBar { <init>(...); }

# view AndroidManifest.xml #generated:90
-keep class com.slidingmenu.example.ViewPagerActivity { <init>(...); }

# view AndroidManifest.xml #generated:78
-keep class com.slidingmenu.example.anim.CustomRotateAnimation { <init>(...); }

# view AndroidManifest.xml #generated:72
-keep class com.slidingmenu.example.anim.CustomScaleAnimation { <init>(...); }

# view AndroidManifest.xml #generated:81
-keep class com.slidingmenu.example.anim.CustomSlideAnimation { <init>(...); }

# view AndroidManifest.xml #generated:75
-keep class com.slidingmenu.example.anim.CustomZoomAnimation { <init>(...); }

# view AndroidManifest.xml #generated:128
-keep class com.slidingmenu.example.fragments.ChangeMobileActivity { <init>(...); }

# view AndroidManifest.xml #generated:107
-keep class com.slidingmenu.example.fragments.ChatActivity { <init>(...); }

# view AndroidManifest.xml #generated:189
-keep class com.slidingmenu.example.fragments.CheckActivity { <init>(...); }

# view AndroidManifest.xml #generated:181
-keep class com.slidingmenu.example.fragments.CropImageActivity { <init>(...); }

# view AndroidManifest.xml #generated:96
-keep class com.slidingmenu.example.fragments.CustomActivity { <init>(...); }

# view AndroidManifest.xml #generated:185
-keep class com.slidingmenu.example.fragments.ExitActivity { <init>(...); }

# view AndroidManifest.xml #generated:176
-keep class com.slidingmenu.example.fragments.ExitFromSettings { <init>(...); }

# view AndroidManifest.xml #generated:140
-keep class com.slidingmenu.example.fragments.FindMemberActivity { <init>(...); }

# view AndroidManifest.xml #generated:166
-keep class com.slidingmenu.example.fragments.FirstSetPasswordActivity { <init>(...); }

# view AndroidManifest.xml #generated:93
-keep class com.slidingmenu.example.fragments.FragmentChangeActivity { <init>(...); }

# view AndroidManifest.xml #generated:197
-keep class com.slidingmenu.example.fragments.ImageActivity { <init>(...); }

# view AndroidManifest.xml #generated:111
-keep class com.slidingmenu.example.fragments.ImageResultActivity { <init>(...); }

# view res/layout/main_address_list.xml #generated:154
# view res/layout/select_member_list.xml #generated:123
-keep class com.slidingmenu.example.fragments.LetterListView { <init>(...); }

# view AndroidManifest.xml #generated:59
-keep class com.slidingmenu.example.fragments.LoadingActivity { <init>(...); }

# view AndroidManifest.xml #generated:103
-keep class com.slidingmenu.example.fragments.MessageTopDialog { <init>(...); }

# view AndroidManifest.xml #generated:119
-keep class com.slidingmenu.example.fragments.PersonalProfileActivity { <init>(...); }

# view AndroidManifest.xml #generated:99
-keep class com.slidingmenu.example.fragments.ResponsiveUIActivity { <init>(...); }

# view AndroidManifest.xml #generated:115
-keep class com.slidingmenu.example.fragments.ScheduleAddActivity { <init>(...); }

# view AndroidManifest.xml #generated:158
-keep class com.slidingmenu.example.fragments.ScheduleMeetingActivity { <init>(...); }

# view AndroidManifest.xml #generated:162
-keep class com.slidingmenu.example.fragments.ScheduleOtherActivity { <init>(...); }

# view AndroidManifest.xml #generated:154
-keep class com.slidingmenu.example.fragments.ScheduleTaskActivity { <init>(...); }

# view AndroidManifest.xml #generated:136
-keep class com.slidingmenu.example.fragments.SelectMemberActivity { <init>(...); }

# view AndroidManifest.xml #generated:193
-keep class com.slidingmenu.example.fragments.SetPasswordActivity { <init>(...); }

# view AndroidManifest.xml #generated:171
-keep class com.slidingmenu.example.fragments.SetPersonalProfileActivity { <init>(...); }

# view AndroidManifest.xml #generated:150
-keep class com.slidingmenu.example.fragments.SetSignatureActivity { <init>(...); }

# view AndroidManifest.xml #generated:145
-keep class com.slidingmenu.example.fragments.SettingActivity { <init>(...); }

# view AndroidManifest.xml #generated:123
-keep class com.slidingmenu.example.fragments.TaskChatActivity { <init>(...); }

# view AndroidManifest.xml #generated:132
-keep class com.slidingmenu.example.fragments.TaskMemberActivity { <init>(...); }

# view res/layout/gl_modify_avatar.xml #generated:13
-keep class com.slidingmenu.example.utils.CropImageView { <init>(...); }

# view res/layout/menu_list.xml #generated:77
-keep class com.slidingmenu.example.utils.ImgButton { <init>(...); }

# view res/layout/chat_message.xml #generated:206
# view res/layout/company_news.xml #generated:41
# view res/layout/user_feedback_chat_message.xml #generated:176
-keep class com.slidingmenu.example.utils.PullToRefreshListView { <init>(...); }

# view AndroidManifest.xml #generated:206
-keep class com.slidingmenu.example.utils.PushMessageReceiver { <init>(...); }

# view res/layout/time_layout.xml #generated:49
# view res/layout/time_layout.xml #generated:55
# view res/layout/time_layout.xml #generated:61
# view res/layout/time_layout.xml #generated:67
# view res/layout/time_layout.xml #generated:73
-keep class com.slidingmenu.example.utils.WheelView { <init>(...); }

# view res/layout/slidingmenumain.xml #generated:2
-keep class com.slidingmenu.lib.SlidingMenu { <init>(...); }

# onClick res/layout/task_chat.xml #generated:14
-keepclassmembers class * { *** backToTask(...); }

# onClick res/layout/personal_profile.xml #generated:14
# onClick res/layout/task_member.xml #generated:14
-keepclassmembers class * { *** backtoChat(...); }

# onClick res/layout/add_friend_layout.xml #generated:32
-keepclassmembers class * { *** beginSearchMember(...); }

# onClick res/layout/personal_profile.xml #generated:146
-keepclassmembers class * { *** callUp(...); }

# onClick res/layout/set_password_layout.xml #generated:15
-keepclassmembers class * { *** cancelSetPassword(...); }

# onClick res/layout/set_signature_layout.xml #generated:14
-keepclassmembers class * { *** cancelSetSignature(...); }

# onClick res/layout/chat_message.xml #generated:15
-keepclassmembers class * { *** chatBack(...); }

# onClick res/layout/change_mobile_layout.xml #generated:32
-keepclassmembers class * { *** checkModifyMobile(...); }

# onClick res/layout/personal_profile.xml #generated:43
# onClick res/layout/set_password_layout.xml #generated:33
-keepclassmembers class * { *** checkSetPassword(...); }

# onClick res/layout/set_signature_layout.xml #generated:32
-keepclassmembers class * { *** checkSetSignature(...); }

# onClick res/layout/settings.xml #generated:212
-keepclassmembers class * { *** exit_settings(...); }

# onClick res/layout/exit_dialog.xml #generated:32
# onClick res/layout/exit_dialog_from_group.xml #generated:29
# onClick res/layout/exit_dialog_from_settings.xml #generated:29
-keepclassmembers class * { *** exitbutton0(...); }

# onClick res/layout/exit_dialog.xml #generated:43
# onClick res/layout/exit_dialog_from_group.xml #generated:43
# onClick res/layout/exit_dialog_from_settings.xml #generated:43
-keepclassmembers class * { *** exitbutton1(...); }

# onClick res/layout/settings.xml #generated:15
-keepclassmembers class * { *** finishSetting(...); }

# onClick res/layout/task_chat.xml #generated:32
-keepclassmembers class * { *** groupMember(...); }

# onClick res/layout/image_select.xml #generated:52
# onClick res/layout/login.xml #generated:56
-keepclassmembers class * { *** loginRequest(...); }

# onClick res/layout/first_set_password_layout.xml #generated:23
-keepclassmembers class * { *** nextStep(...); }

# onClick res/layout/bargain_price.xml #generated:14
# onClick res/layout/company_news.xml #generated:14
# onClick res/layout/fragment_settings.xml #generated:15
# onClick res/layout/main_address_list.xml #generated:22
# onClick res/layout/main_friends.xml #generated:14
# onClick res/layout/main_message.xml #generated:16
# onClick res/layout/schedule.xml #generated:14
# onClick res/layout/user_feedback_chat_message.xml #generated:15
-keepclassmembers class * { *** openMainMenu(...); }

# onClick res/layout/chat_message.xml #generated:64
-keepclassmembers class * { *** openTaskMember(...); }

# onClick res/layout/add_friend_layout.xml #generated:14
-keepclassmembers class * { *** quitActivity(...); }

# onClick res/layout/change_mobile_layout.xml #generated:14
-keepclassmembers class * { *** quitChangeMobile(...); }

# onClick res/layout/task_member.xml #generated:121
-keepclassmembers class * { *** quitFromGroup(...); }

# onClick res/layout/personal_profile.xml #generated:162
-keepclassmembers class * { *** sendMessage(...); }

# onClick res/layout/personal_profile.xml #generated:32
-keepclassmembers class * { *** setPersonalInfo(...); }

# onClick res/layout/fragment_settings.xml #generated:44
# onClick res/layout/settings.xml #generated:44
-keepclassmembers class * { *** settings_information(...); }

# onClick res/layout/check_dialog.xml #generated:50
-keepclassmembers class * { *** toContinue(...); }

# onClick res/layout/set_personal_profile.xml #generated:23
-keepclassmembers class * { *** toSkip(...); }

# onClick res/layout/schedule_add.xml #generated:34
-keepclassmembers class * { *** toTaskChat(...); }

