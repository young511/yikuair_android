package com.slidingmenu.example.fragments;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.slidingmenu.example.R;
import com.slidingmenu.example.database.DBOpenHelper;
import com.slidingmenu.example.utils.ImgButton;
import com.slidingmenu.example.utils.MessageInfo;
import com.slidingmenu.example.utils.SharedPreferencesUtil;
import com.slidingmenu.example.utils.UserInfo;

public class MenuFragment extends Fragment {

	private TextView staffName;
	private TextView jobPosition;
	private TextView department;
	private ListView menuList;
	private listAdapter listAda;
	private static final int COMPANY_NEWS = 0;
	private static final int COMPANY_SCHEDULE = 1;
	private static final int COMPANY_MESSAGE = 2;
	private static final int COMPANY_ADDRESS_LIST = 3;
	private static final int COMPANY_USER_FEEDBACK = 4;
	private int unReadedMessageCount;
	public static MenuFragment instance = null;
	public SharedPreferencesUtil menuShared = null;
	public static final String MENUNAME = "MenuName";
	/*
	 * private static final int COMPANY_APP = 4; private static final int
	 * COMPANY_SALES = 5; private static final int COMPANY_FRIENDS = 6; private
	 * static final int COMPANY_SERVICE = 7; private static final int
	 * GAME_RECOMMEND = 8;
	 */
	private int currentPos;
	private Fragment newContent;
	private View preView;
	private ImgButton setBtn;
	private ImageView staff_photo;
	private RelativeLayout rl_user_info;
	private ChatBroadcastReceiver cbr = null;
	private HeadModifiedBroadcastReceiver hbr = null;
	private List<MenuItemInfo> list = new ArrayList<MenuItemInfo>();
	private HashMap<Integer, List<ChatMsgEntity>> msgMap = new HashMap<Integer, List<ChatMsgEntity>>();
	private DBOpenHelper dbOpenHelper;
	private Cursor cursor;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.menu_list, null);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		menuShared = new SharedPreferencesUtil(getActivity());
		currentPos = COMPANY_MESSAGE;
		initView();

		getMenuDataFromShared();

		Log.i("test", "menu onActivityCreated ...............");
	}

	public void initView() {
		Log.e("test", "menu initView....");
		SharedPreferencesUtil shared = new SharedPreferencesUtil(getActivity());
		shared.getUserInfo();
		shared.getPhotoUrl();

		rl_user_info = (RelativeLayout) getActivity().findViewById(
				R.id.user_info);
		rl_user_info.setOnClickListener(btnClick);
		staffName = (TextView) getActivity().findViewById(R.id.staff_name);
		jobPosition = (TextView) getActivity().findViewById(R.id.job_position);
		department = (TextView) getActivity().findViewById(R.id.job_department);
		staff_photo = (ImageView) getActivity().findViewById(R.id.staff_photo);
		menuList = (ListView) getActivity().findViewById(R.id.m_list);
		listAda = new listAdapter(getActivity(), list);
		menuList.setAdapter(listAda);

		if (UserInfo.LocalphotoPath != null
				&& UserInfo.LocalphotoPath.length() > 0) {
			Bitmap b = BitmapFactory.decodeFile(UserInfo.LocalphotoPath);
			staff_photo.setBackgroundDrawable(new BitmapDrawable(b));
		} else {
			if (UserInfo.sex.equals("0"))
				staff_photo.setBackgroundDrawable(getResources().getDrawable(
						R.drawable.girl));
			else
				staff_photo.setBackgroundDrawable(getResources().getDrawable(
						R.drawable.boy));
		}

		staffName.setText(UserInfo.realName);
		jobPosition.setText(UserInfo.duty);
		department.setText(UserInfo.team);

		setBtn = (ImgButton) getActivity().findViewById(R.id.set_btn);
		setBtn.setOnClickListener(btnClick);

		list.add(new MenuItemInfo(getActivity().getResources().getString(
				R.string.company_news), 0, View.GONE, R.drawable.left_0_ico));
		list.add(new MenuItemInfo(getActivity().getResources().getString(
				R.string.company_agenda), 0, View.GONE, R.drawable.left_1_ico));
		list.add(new MenuItemInfo(getActivity().getResources().getString(
				R.string.company_message), 0, View.GONE, R.drawable.left_2_ico));
		list.add(new MenuItemInfo(getActivity().getResources().getString(
				R.string.company_address_list), 0, View.GONE,
				R.drawable.left_3_ico));
		list.add(new MenuItemInfo(getActivity().getResources().getString(
				R.string.user_feedback), 0, View.GONE, R.drawable.left_4_ico));

		Log.i("test", "UserInfo.isfirstlogin... :" + UserInfo.isFirstLogin);
		Log.i("test", "UserInfo.feedback... :" + UserInfo.feedback_dbId);
		Log.i("test", "UserInfo.copanynewdbid... :" + UserInfo.companyNews_dbId);

		if (isFirstLogin()) {
			ChatMsgEntity entity = new ChatMsgEntity();
			entity.setIsComing(true);
			entity.setContent(getActivity().getResources().getString(
					R.string.user_feedback_first_message));
			entity.setType(MessageInfo.TEXT);
			entity.setTime(MessageInfo.getChatTime());
			entity.setSenderId(UserInfo.feedback_dbId);
			entity.setReceiverId(UserInfo.feedback_dbId);
			entity.setStatus(MessageInfo.RECEIVE_MESSAGE);
			updataMenuList(COMPANY_USER_FEEDBACK, 1, entity);

			ChatMsgEntity ent = new ChatMsgEntity();
			ent.setIsComing(true);
			ent.setContent(getActivity().getResources().getString(
					R.string.welcome_news));
			ent.setType(MessageInfo.TEXT);
			ent.setTime(MessageInfo.getChatTime());
			ent.setSenderId(UserInfo.companyNews_dbId);
			ent.setReceiverId(UserInfo.companyNews_dbId);
			ent.setStatus(MessageInfo.RECEIVE_MESSAGE);
			updataMenuList(COMPANY_NEWS, 1, ent);
			/*********************************************/
			/*
			 * MessageInfo.messageEntityList.add(entity);
			 * MessageInfo.messageEntityList.add(ent);
			 */
		}
		menuList.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {

				currentPos = arg2;
				if (preView != null) {
					preView.setBackgroundColor(Color.TRANSPARENT);
					ImageView leftBar = (ImageView) preView
							.findViewById(R.id.iv_left_bar);
					leftBar.setVisibility(View.INVISIBLE);
					TextView tvItem = (TextView) preView
							.findViewById(R.id.item_function);
					tvItem.setTextColor(getResources().getColor(
							R.color.menu_item1_tv_normal));
					ImageView rightBar = (ImageView) preView
							.findViewById(R.id.iv_right_bar);
					rightBar.setBackgroundResource(R.drawable.mm_submenu_normal);

					ImageView leftIcon = (ImageView) preView
							.findViewById(R.id.iv_left_icon);

					if (tvItem
							.getText()
							.toString()
							.equals(getActivity().getResources().getString(
									R.string.company_news))) {
						leftIcon.setBackgroundResource(R.drawable.left_0_ico);
					} else if (tvItem
							.getText()
							.toString()
							.equals(getActivity().getResources().getString(
									R.string.company_agenda))) {
						leftIcon.setBackgroundResource(R.drawable.left_1_ico);
					} else if (tvItem
							.getText()
							.toString()
							.equals(getActivity().getResources().getString(
									R.string.company_message))) {
						leftIcon.setBackgroundResource(R.drawable.left_2_ico);
					} else if (tvItem
							.getText()
							.toString()
							.equals(getActivity().getResources().getString(
									R.string.company_address_list))) {
						leftIcon.setBackgroundResource(R.drawable.left_3_ico);
					} else if (tvItem
							.getText()
							.toString()
							.equals(getActivity().getResources().getString(
									R.string.user_feedback))) {
						leftIcon.setBackgroundResource(R.drawable.left_4_ico);
					}
				}
				arg1.setBackgroundColor(Color.BLACK);

				ImageView leftBar = (ImageView) arg1
						.findViewById(R.id.iv_left_bar);
				leftBar.setVisibility(View.VISIBLE);

				ImageView leftIcon = (ImageView) arg1
						.findViewById(R.id.iv_left_icon);

				TextView tvItem = (TextView) arg1
						.findViewById(R.id.item_function);
				tvItem.setTextColor(Color.WHITE);

				ImageView rightBar = (ImageView) arg1
						.findViewById(R.id.iv_right_bar);
				rightBar.setBackgroundResource(R.drawable.mm_submenu_pressed);

				if (tvItem
						.getText()
						.toString()
						.equals(getActivity().getResources().getString(
								R.string.company_news))) {
					leftIcon.setBackgroundResource(R.drawable.left_0_ico_highlighted);
				} else if (tvItem
						.getText()
						.toString()
						.equals(getActivity().getResources().getString(
								R.string.company_agenda))) {
					leftIcon.setBackgroundResource(R.drawable.left_1_ico_highlighted);
				} else if (tvItem
						.getText()
						.toString()
						.equals(getActivity().getResources().getString(
								R.string.company_message))) {
					leftIcon.setBackgroundResource(R.drawable.left_2_ico_highlighted);
				} else if (tvItem
						.getText()
						.toString()
						.equals(getActivity().getResources().getString(
								R.string.company_address_list))) {
					leftIcon.setBackgroundResource(R.drawable.left_3_ico_highlighted);
				} else if (tvItem
						.getText()
						.toString()
						.equals(getActivity().getResources().getString(
								R.string.user_feedback))) {
					leftIcon.setBackgroundResource(R.drawable.left_4_ico_highlighted);
				}

				preView = arg1;
				currentPos = arg2;

				newContent = null;
				switch (arg2) {
				case COMPANY_NEWS:
					if (msgMap.get(COMPANY_NEWS) != null) {
						updataMenuList(COMPANY_NEWS, 0, null);
						for (int i = 0; i < msgMap.get(COMPANY_NEWS).size(); i++) {
							MessageInfo.menuCompanyNewsList.add(msgMap.get(
									COMPANY_NEWS).get(i));
						}
						msgMap.get(COMPANY_NEWS).clear();
					}
					list.get(COMPANY_NEWS).setNum(0);
					list.get(COMPANY_NEWS).setNumVisible(View.GONE);
					newContent = new CompanyNewsFragment();
					break;
				case COMPANY_SCHEDULE:
					newContent = new ScheduleFragment();
					break;
				case COMPANY_MESSAGE:
					Log.e("test", "messageintitylist.size :"
							+ MessageInfo.messageEntityList.size());

					if (msgMap.get(COMPANY_MESSAGE) != null) {
						updataMenuList(COMPANY_MESSAGE, 0, null);
						for (int i = 0; i < msgMap.get(COMPANY_MESSAGE).size(); i++) {
							MessageInfo.messageEntityList.add(msgMap.get(
									COMPANY_MESSAGE).get(i));
						}
						msgMap.get(COMPANY_MESSAGE).clear();
					}
					list.get(COMPANY_MESSAGE).setNum(0);
					list.get(COMPANY_MESSAGE).setNumVisible(View.GONE);

					newContent = new MessageFragment();
					break;
				case COMPANY_ADDRESS_LIST:
					newContent = new PhoneBookFragment();
					break;
				case COMPANY_USER_FEEDBACK:
					if (msgMap.get(COMPANY_USER_FEEDBACK) != null) {
						updataMenuList(COMPANY_USER_FEEDBACK, 0, null);
						for (int i = 0; i < msgMap.get(COMPANY_USER_FEEDBACK)
								.size(); i++) {
							MessageInfo.menuFeedbackList.add(msgMap.get(
									COMPANY_USER_FEEDBACK).get(i));
						}
						msgMap.get(COMPANY_USER_FEEDBACK).clear();
					}

					list.get(COMPANY_USER_FEEDBACK).setNum(0);
					list.get(COMPANY_USER_FEEDBACK).setNumVisible(View.GONE);
					newContent = new FeedBackFragment();
					break;
				/*
				 * case COMPANY_SALES: newContent = new BargainPriceFragment();
				 * break; case COMPANY_FRIENDS: newContent = new
				 * MakeFriendsFragment(); break; case COMPANY_SERVICE:
				 * 
				 * break; case GAME_RECOMMEND:
				 * 
				 * break;
				 */
				}
				if (newContent != null)
					switchFragment(newContent);
			}
		});
	}

	public void readDataFromLocal() {

		Log.e("test", "size :" + MessageInfo.messageEntityList.size());

		if (MessageInfo.messageEntityList != null
				&& !MessageInfo.messageEntityList.isEmpty()) {

			for (int i = 0; i < MessageInfo.messageEntityList.size(); i++) {
				int menuItem = -1;

				if (MessageInfo.messageEntityList.get(i).getStatus() != MessageInfo.SEND_MESSAGE
						&& MessageInfo.messageEntityList.get(i).getStatus() != MessageInfo.RECEIVE_MESSAGE
						&& MessageInfo.messageEntityList.get(i).getStatus() != MessageInfo.GROUP_MODIFY)
					continue;

				Log.e("test", "receiver  ss :"
						+ MessageInfo.messageEntityList.get(i).getReceiverId());
				if ((MessageInfo.messageEntityList.get(i).getStatus() == MessageInfo.SEND_MESSAGE)
						|| (MessageInfo.messageEntityList.get(i).getChatType() == MessageInfo.GROUP && MessageInfo.groupMap
								.containsKey(MessageInfo.messageEntityList.get(
										i).getReceiverId()))
						|| MessageInfo.messageEntityList.get(i).getReceiverId()
								.equals(UserInfo.db_id)
								|| MessageInfo.messageEntityList.get(i).getStatus() == MessageInfo.GROUP_MODIFY) {
					Log.e("test", "company message..........");
					menuItem = COMPANY_MESSAGE;
				} else if (MessageInfo.messageEntityList.get(i).getSenderId()
						.equals(UserInfo.companyNews_dbId)) {
					Log.e("test", "company news.........");
					menuItem = COMPANY_NEWS;
				} else if (MessageInfo.messageEntityList.get(i).getSenderId()
						.equals(UserInfo.feedback_dbId)) {
					Log.e("test", "company user feedback............");
					menuItem = COMPANY_USER_FEEDBACK;
				}
				int count = 0;
				if (MessageInfo.messageEntityList.get(i).getStatus() != MessageInfo.SEND_MESSAGE)// send
																									// message
																									// is
																									// not
																									// show
																									// count
					count = 1;
				Log.e("test", "count is " + count);
				updataMenuList(menuItem, count,
						MessageInfo.messageEntityList.get(i));
			}
			MessageInfo.messageEntityList.clear();
		}
	}

	/*******************************************/
	public void getMenuDataFromShared() {
		if (menuShared == null)
			menuShared = new SharedPreferencesUtil(getActivity());
		if (menuShared.readMenuDataFromShared(MENUNAME + UserInfo.db_id) != null) {
			msgMap = menuShared.readMenuDataFromShared(MENUNAME
					+ UserInfo.db_id);
			if (msgMap != null && !msgMap.isEmpty()) {
				MessageInfo.messageEntityList.clear();
				Set set = msgMap.keySet();
				Iterator it = set.iterator();

				while (it.hasNext()) {
					int key = (Integer) it.next();
					List<ChatMsgEntity> list = msgMap.get(key);
					if (list != null && !list.isEmpty())
						for (int i = 0; i < list.size(); i++) {
							MessageInfo.messageEntityList.add(list.get(i));
						}
				}
				msgMap.clear();
				readDataFromLocal();
			}
		}
	}

	public class ChatBroadcastReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			Log.i("test", "menu broadcast receive...................");
			ChatMsgEntity entity = new ChatMsgEntity();
			Bundle bundle = intent.getExtras();
			Serializable data = bundle.getSerializable("message");
			if (data != null) {
				entity = (ChatMsgEntity) data;
				if (entity.getStatus() != MessageInfo.RECEIVE_MESSAGE
						&& entity.getStatus() != MessageInfo.GROUP_MODIFY)
					return;
				int itemNum = -1;
				if (entity.getSenderId().equals(UserInfo.companyNews_dbId))
					itemNum = COMPANY_NEWS;
				else if (entity.getSenderId().equals(UserInfo.feedback_dbId))
					itemNum = COMPANY_USER_FEEDBACK;
				else if (entity.getChatType() == MessageInfo.GROUP
						&& MessageInfo.groupMap.containsKey(entity
								.getReceiverId())
						|| entity.getReceiverId().equals(UserInfo.db_id)) {
					itemNum = COMPANY_MESSAGE;
				}

				Log.e("test", "companyNews_dbid :" + UserInfo.companyNews_dbId);
				Log.e("test", "companyfeedback_dbid :" + UserInfo.feedback_dbId);
				Log.e("test", "id_id :" + UserInfo.db_id);

				Log.i("test", "from :" + entity.getSenderId());
				Log.i("test", "itemnum : " + itemNum);
				Log.i("test", "to :" + entity.getReceiverId());
				updataMenuList(itemNum, 1, entity);
			} else {
				// Log.e("test", "count is " +
				// MessageInfo.unReadedMessageCount);
				String name = intent.getStringExtra("name");
				if (name != null && name.length() > 0)
					if (name.equals(getActivity().getResources().getString(
							R.string.company_news))) {
						list.get(COMPANY_NEWS).setNum(0);
						list.get(COMPANY_NEWS).setNumVisible(View.GONE);
					} else if (name.equals(getActivity().getResources()
							.getString(R.string.company_message))) {
						unReadedMessageCount = MessageInfo.unReadedMessageCount;
						Log.e("test", "unreaded count is "
								+ unReadedMessageCount);
						if (unReadedMessageCount > 0) {
							list.get(COMPANY_MESSAGE).setNum(
									unReadedMessageCount);
							list.get(COMPANY_MESSAGE).setNumVisible(
									View.VISIBLE);
						} else {
							list.get(COMPANY_MESSAGE).setNum(0);
							list.get(COMPANY_MESSAGE).setNumVisible(View.GONE);
						}
					} else if (name.equals(getActivity().getResources()
							.getString(R.string.user_feedback))) {
						list.get(COMPANY_USER_FEEDBACK).setNum(0);
						list.get(COMPANY_USER_FEEDBACK)
								.setNumVisible(View.GONE);
					}

				listAda.notifyDataSetChanged();
				// updataMenuList(COMPANY_MESSAGE, unReadedMessageCount, null);
			}
		}
	}

	public class HeadModifiedBroadcastReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			Log.e("test", "menu broadcast receive...................");
			if (UserInfo.LocalphotoPath != null
					&& UserInfo.LocalphotoPath.length() > 0) {
				Bitmap b = BitmapFactory.decodeFile(UserInfo.LocalphotoPath);
				staff_photo.setBackgroundDrawable(new BitmapDrawable(b));
			}
		}
	}

	private OnClickListener btnClick = new OnClickListener() {
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.user_info:
				Intent intent = new Intent(getActivity(),
						PersonalProfileActivity.class);
				intent.putExtra("individualInfo", " ");
				startActivity(intent);
				getActivity().overridePendingTransition(R.anim.in_from_right,
						R.anim.out_of_left);
				break;
			case R.id.set_btn:/*
							 * Intent intent2 = new Intent(getActivity(),
							 * SettingActivity.class); startActivity(intent2);
							 * getActivity
							 * ().overridePendingTransition(R.anim.in_from_right
							 * , R.anim.out_of_left);
							 */
				newContent = new SettingFragment();
				switchFragment(newContent);
			}
		}
	};

	private boolean isFirstLogin() {
		SharedPreferences setting = getActivity().getSharedPreferences(
				"yikuairMenu", 0);
		Boolean user_first = setting.getBoolean("FIRST", true);
		if (user_first) {
			setting.edit().putBoolean("FIRST", false).commit();
			UserInfo.isFirstLogin = true;
			return true;
		} else {
			UserInfo.isFirstLogin = false;
			return false;
		}
	}

	public void onStart() {
		super.onStart();
		Log.i("test", "menu onstart....................");
		// SharedPreferencesUtil shared = new
		// SharedPreferencesUtil(getActivity());
		// shared.getUserInfo();

		dbOpenHelper = new DBOpenHelper(getActivity());

		if (UserInfo.companyNews_dbId == null
				|| UserInfo.companyNews_dbId.length() == 0) {
			UserInfo.companyNews_dbId = getDBIdFromDB(getActivity()
					.getResources().getString(R.string.company_news));
			Log.e("test", "...........company dbid :"
					+ UserInfo.companyNews_dbId);
		}
		if (UserInfo.feedback_dbId == null
				|| UserInfo.feedback_dbId.length() == 0) {
			UserInfo.feedback_dbId = getDBIdFromDB(getActivity().getResources()
					.getString(R.string.user_feedback));
			Log.e("test", "...................feedback dbid :"
					+ UserInfo.feedback_dbId);
		}

		// register broadcast
		if (cbr == null) {
			IntentFilter myIntentFilter = new IntentFilter();
			myIntentFilter.addAction(MessageInfo.MessageBroadCastName);
			cbr = new ChatBroadcastReceiver();
			getActivity().registerReceiver(cbr, myIntentFilter);
		}
		if (hbr == null) {
			IntentFilter headIntentFilter = new IntentFilter();
			headIntentFilter.addAction(MessageInfo.HeadModifieyBroadCastName);
			hbr = new HeadModifiedBroadcastReceiver();
			getActivity().registerReceiver(hbr, headIntentFilter);
		}
		readDataFromLocal();

		if (UserInfo.LocalphotoPath != null
				&& UserInfo.LocalphotoPath.length() > 0) {
			Bitmap b = BitmapFactory.decodeFile(UserInfo.LocalphotoPath);
			staff_photo.setBackgroundDrawable(new BitmapDrawable(b));
		}

		instance = this;
	}

	public void onResume() {
		Log.i("test", "menu onResume....................");
		super.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
		Log.i("test", "menu onPause");
	}

	@Override
	public void onStop() {
		super.onStop();
		SharedPreferencesUtil shared = new SharedPreferencesUtil(getActivity());
		shared.saveUserInfo();
		if (cbr != null) {
			getActivity().unregisterReceiver(cbr);
			cbr = null;
		}
		instance = null;

		/*
		 * if(menuShared == null) menuShared = new
		 * SharedPreferencesUtil(getActivity());
		 * 
		 * Set set = msgMap.keySet(); Iterator it = set.iterator(); int i = 0;
		 * while (it.hasNext()) { int key = (Integer) it.next(); for(i = 0; i <
		 * msgMap.get(key).size(); i++){
		 * MessageInfo.messageEntityList.add(msgMap.get(key).get(i)); } }
		 * 
		 * menuShared.saveMenuDatatoShared(MENUNAME + UserInfo.db_id,
		 * MessageInfo.messageEntityList);
		 * 
		 * Log.e("test", "messageentitylist size is " +
		 * MessageInfo.messageEntityList.size());
		 */Log.e("test", "menu  onStop");
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (hbr != null) {
			getActivity().unregisterReceiver(hbr);
			hbr = null;
		}

		if (menuShared == null)
			menuShared = new SharedPreferencesUtil(getActivity());

		menuShared.saveMenuDatatoShared(msgMap, MENUNAME + UserInfo.db_id);

		Log.i("test", "menu onDestroy");
	}

	// the meat of switching the above fragment
	private void switchFragment(Fragment fragment) {
		if (getActivity() == null)
			return;
		if (getActivity() instanceof ResponsiveUIActivity) {
			ResponsiveUIActivity ra = (ResponsiveUIActivity) getActivity();
			ra.switchContent(fragment);
		}
	}

	private void updataMenuList(int menuItemNum, int messageCount,
			ChatMsgEntity entity) {

		if (menuItemNum == -1)
			return;

		if (msgMap.containsKey(menuItemNum)) {
			if (entity != null) {
				if (MessageFragment.instance == null) {
					if (entity.getStatus() != MessageInfo.SEND_MESSAGE) {
						entity.setIsAdd(true);
					}
				} else {
					entity.setIsAdd(false);
				}
				if (menuItemNum == COMPANY_NEWS) {
					if (CompanyNewsFragment.instance == null)
						msgMap.get(menuItemNum).add(entity);
				} else if (menuItemNum == COMPANY_USER_FEEDBACK) {
					if (FeedBackFragment.instance == null)
						msgMap.get(menuItemNum).add(entity);
				} else
					msgMap.get(menuItemNum).add(entity);
				list.get(menuItemNum)
						.setNum(list.get(menuItemNum).getNum()
								+ messageCount
								+ (entity.getStatus() == MessageInfo.GROUP_MODIFY ? -1
										: 0));
				if (list.get(menuItemNum).getNum()
						+ messageCount
						+ (entity.getStatus() == MessageInfo.GROUP_MODIFY ? -1
								: 0) > 0)
					list.get(menuItemNum).setNumVisible(View.VISIBLE);
				else
					list.get(menuItemNum).setNumVisible(View.GONE);
			} else {
				if (messageCount == 0) {
					list.get(menuItemNum).setNumVisible(View.GONE);
				} else {
					list.get(menuItemNum).setNum(messageCount);
					list.get(menuItemNum).setNumVisible(View.VISIBLE);
				}
			}
		} else {
			List<ChatMsgEntity> msgList = new ArrayList<ChatMsgEntity>();
			if (entity != null) {
				if (MessageFragment.instance == null) {
					if (entity.getStatus() != MessageInfo.SEND_MESSAGE) {
						entity.setIsAdd(true);
					}
				} else {
					entity.setIsAdd(false);
				}
				msgList.add(entity);
				msgMap.put(menuItemNum, msgList);
				int count = 0;
				for (int i = 0; i < msgMap.get(menuItemNum).size(); i++) {
					if (msgMap.get(menuItemNum).get(i).getStatus() != MessageInfo.SEND_MESSAGE)
						count++;
				}
				if (count > 0/* msgMap.get(menuItemNum).size() > 0 */) {
					Log.e("test", "count is    + "
							+ msgMap.get(menuItemNum).size());
					list.get(menuItemNum)
							.setNum(list.get(menuItemNum).getNum()
									+ msgMap.get(menuItemNum).size()
									+ (entity.getStatus() == MessageInfo.GROUP_MODIFY ? -1
											: 0));
					if (list.get(menuItemNum).getNum()
							+ msgMap.get(menuItemNum).size()
							+ (entity.getStatus() == MessageInfo.GROUP_MODIFY ? -1
									: 0) > 0)
						list.get(menuItemNum).setNumVisible(View.VISIBLE);
					else
						list.get(menuItemNum).setNumVisible(View.GONE);
				} else
					list.get(menuItemNum).setNumVisible(View.GONE);
			}
		}
		listAda.notifyDataSetChanged();
	}

	public String getHeadUrlFromDB(String dbId) {
		if (dbId == null)
			return "";
		dbOpenHelper = new DBOpenHelper(getActivity());
		SQLiteDatabase db = dbOpenHelper.getReadableDatabase();
		String name = "";
		cursor = db.rawQuery("select * from contactsTable where dbid=?",
				new String[] { dbId });
		if (cursor.moveToFirst()) {
			name = cursor.getString(8);
		}
		dbOpenHelper.close();
		cursor.close();
		return name;
	}

	public String getSexFromDB(String dbId) {
		if (dbId == null)
			return "";
		dbOpenHelper = new DBOpenHelper(getActivity());
		SQLiteDatabase db = dbOpenHelper.getReadableDatabase();
		String name = "";
		cursor = db.rawQuery("select * from contactsTable where dbid=?",
				new String[] { dbId });
		if (cursor.moveToFirst()) {
			name = cursor.getString(13);
		}
		dbOpenHelper.close();
		cursor.close();
		return name;
	}

	private String getDBIdFromDB(String name) {
		if (name == null)
			return "";
		String dbid = "";
		dbOpenHelper = new DBOpenHelper(getActivity());
		SQLiteDatabase db = dbOpenHelper.getReadableDatabase();
		cursor = db.rawQuery("select * from contactsTable where name=?",
				new String[] { name });
		if (cursor.moveToFirst()) {
			dbid = cursor.getString(14);
		}
		cursor.close();
		dbOpenHelper.close();
		return dbid;
	}

	private class listAdapter extends BaseAdapter {
		private List<MenuItemInfo> mData;
		private LayoutInflater mInflater;

		public listAdapter(Context context, List<MenuItemInfo> list) {
			mInflater = LayoutInflater.from(context);
			mData = list;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return mData.size();
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return mData.get(arg0);
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return arg0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			ViewHolder holder = null;
			holder = new ViewHolder();
			convertView = mInflater.inflate(R.layout.menu_item1, null);
			holder.textView = (TextView) convertView
					.findViewById(R.id.item_function);
			holder.rl_num = (RelativeLayout) convertView
					.findViewById(R.id.rl_prompt_num);
			holder.tv_num = (TextView) convertView
					.findViewById(R.id.tv_prompt_num);
			holder.iv_icon = (ImageView) convertView
					.findViewById(R.id.iv_left_icon);

			holder.textView.setText(mData.get(position).getMenuName());
			holder.rl_num.setVisibility(mData.get(position).getNumVisible());
			holder.tv_num.setText(String.valueOf(mData.get(position).getNum()));
			holder.iv_icon.setBackgroundResource(mData.get(position).getIcon());

			if (position == currentPos) {
				convertView.setBackgroundColor(Color.BLACK);
				ImageView leftBar = (ImageView) convertView
						.findViewById(R.id.iv_left_bar);
				leftBar.setVisibility(View.VISIBLE);

				TextView tvItem = (TextView) convertView
						.findViewById(R.id.item_function);
				tvItem.setTextColor(Color.WHITE);

				ImageView rightBar = (ImageView) convertView
						.findViewById(R.id.iv_right_bar);
				rightBar.setBackgroundResource(R.drawable.mm_submenu_pressed);

				ImageView leftIcon = (ImageView) convertView
						.findViewById(R.id.iv_left_icon);

				if (position == 0) {
					leftIcon.setBackgroundResource(R.drawable.left_0_ico_highlighted);
				} else if (position == 1) {
					leftIcon.setBackgroundResource(R.drawable.left_1_ico_highlighted);
				} else if (position == 2) {
					leftIcon.setBackgroundResource(R.drawable.left_2_ico_highlighted);
				} else if (position == 3) {
					leftIcon.setBackgroundResource(R.drawable.left_3_ico_highlighted);
				} else if (position == 4) {
					leftIcon.setBackgroundResource(R.drawable.left_4_ico_highlighted);
				}
				preView = convertView;
			}
			return convertView;
		}

		public class ViewHolder {
			public TextView textView;
			public RelativeLayout rl_num;
			public TextView tv_num;
			public ImageView iv_icon;
		}
	}
}
